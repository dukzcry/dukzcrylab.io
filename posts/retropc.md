---
title: Дачный ретрокомп
published: 19.07.2021
tags: ретрокомпьютер, slot1
---

Так получилось, что моим первым компьютером стал системный блок с Pentium II. Это была интересная эпоха - процессор в виде картриджа и щелевой разъём на материнке под него, культовые видеоускорители 3dfx, аудиокарты с таблично-волновым синтезом, материнские платы уже с разъёмом AGP, но всё ещё не избавившиеся от ISA слотов.  
Я испытываю сильную ностальгию по тем временам. Однако, я не стал воспроизводить в точности конфигурацию того компьютера, а собрал нечто по его мотивам. Так, под нож пошли форм-фактор AT, разъём клавиатуры DIN5, COM-мышка. Вместо этого была собрана система стандарта ATX, с PS/2 мышью, клавой и USB портами.  
Для проверки софта и игр под данную платформу рекомендую эмулятор 86Box, однако для его запуска потребуется достаточно мощный комп.

$gallery("retropc")$

Итак, что можно собрать:

- Процессор  
Если важна универсальность, то берём Pentium II с разблокированным множителем, что позволит менять частоту процессора. Определить разблокирован ли множитель можно по номеру на картридже. Вторая цифра означает последнюю цифру года, третья и чётвёртая - неделю изготовления. Процессор должен быть изготовлен до 34 недели 98 года  
Если нужна максимальная скорость, то можно брать слотовый Pentium III с частотой 500-600 МГц. Процессоры 800 МГц и выше редки и дороги
- Материнская плата  
Можно приобрести плату стандарта ATX на культовом чипсете 440BX, с набортными USB и PS/2. Желательно с Plug & Play BIOS'ом, чтобы можно было выставлять настройки процессора программно, без джамперов  
В случае выбора данного чипсета стоит с вниманием отнестись к подбору процессора, видеокарты и памяти. Так, штатная частота системной шины у данного чипсета - 100 МГц. В случае разгона по шине лучше выбирать видеокарту от Nvidia, т.к. AMD не все переваривают последующее за разгоном повышение частоты AGP. Память плотностью 256 Мб на планку должна быть двухсторонней
- Оперативная память  
Для девяностовосьмёрки нет необходимости брать больше 512 Мб
- Видеокарта  
Любая AGP карта с 3.3 В разъёмом, либо универсальная
- SATA контроллер  
Полезная опция. Дешевые варианты это Silicon Image 3112/3512. TRIM работать не будет, в остальном неплохая вещь
- SSD диск  
Под 98 винду можно смело брать на 120 Гб, если больше - могут быть проблемы, а меньше теперь, как правило, и не продают
- Сетевая карта  
Беспроводной адаптер TP-LINK TL-WN651G стандарта 802.11g имеет драйвера под Windows 98, и даже работает
- Звуковая карта  
Любая для слота ISA, неважно Plug & Play или нет, но лучше чтобы настройки выставлялись программно, а не перемычками. Если хочется приятного звука в DOS играх, то можно взять карту AWE64
- USB 2.0 контроллер  
Есть смысл поставить для увеличения количества USB слотов и повышения скорости передачи данных, поскольку на материнках той эпохи был лишь медленный USB 1.1. Хорошо будет работать карта на чипе NEC D720100AGM
- CD-RW дисковод  
Подойдёт старенький пишущий дисковод для шины IDE
