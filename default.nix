{ pkgs ? import <nixpkgs> {} }:

let
  haskellPackages = pkgs.haskellPackages.extend (hself: hsuper: {
    hakyll-gallery = hsuper.callCabal2nix "hakyll-gallery" ((pkgs.fetchFromGitLab {
      owner = "dukzcry";
      repo = "funkshun";
      rev = "0b71f4aaf3dcc8ee2918fbe04b4c269c89820704";
      sha256 = "sha256-3rKQL5cV4klSfvpoCuRTufG5rjenPiNsaG5sKGZog1I=";
    }) + "/hakyll-gallery") {};
    site = hsuper.callCabal2nix "site" ./. {};
  });
in pkgs.mkShell {
  packages = [ haskellPackages.site ];
}
